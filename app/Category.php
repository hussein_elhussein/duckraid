<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $hidden = [
        'parent_id',
        'slug',
        'created_at',
        'updated_at',
        'pivot'
    ];
    public function posts(){
      return $this->belongsToMany('App\Post');
    }
    public function scopeMini(Builder $query){
        return $query->select('posts.id','posts.name');
    }
}
