<?php

namespace App\Mail;

use App\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\File;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Inquiry extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    protected $inquiry_type;

    public function __construct(Email $email,$inquiry_type)
    {
        $this->email = $email;
        $this->inquiry_type = $inquiry_type;
    }

    public function build()
    {
        $target = null;
        if($this->inquiry_type === "inquiry"){
            $target = setting('contact-details.inquiry_email');
        }else{
            $target = setting('contact-details.jobs_email');
        }
        $subject = null;
        if($this->email->subject){
            $subject = $this->email->subject;
        }else{
            $subject = 'New email from ' . $this->email->email;
        }

        try{
            if($this->email->attachment){
                $content = Storage::get($this->email->attachment);
                $mime = Storage::mimeType($this->email->attachment);
                $ext = explode('.',$this->email->attachment)[1];
                return $this
                    ->to($target)
                    ->from($this->email->email)
                    ->subject($subject)
                    ->attachData($content, $this->email->subject . '.' . $ext, [
                        'mime' => $mime,
                    ])
                    ->view('emails.inquiry');
            }else{
                return $this
                    ->to($target)
                    ->from($this->email->email)
                    ->subject($subject)
                    ->view('emails.inquiry');
            }
        }catch (\Exception $exception ){
            Log::info($exception);
        }

    }
}
