<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController;
class PostController extends VoyagerBreadController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('edit', app($dataType->model_name));

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->addRows);

      if ($val->fails()) {
        return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->ajax()) {
        $post_model = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        if($request->file('image')){
            $path_arr = explode('/', $post_model->image);
            $ext_arr = explode('.',$post_model->image);
            $ext = end($ext_arr);
            $old_name = end($path_arr);
            $name = str_replace('.' . $ext,'-original.' . $ext,end($path_arr));
            Log::info('image name: ' . $name);
            $folder = str_replace($old_name,'',$post_model->image);
            $request->file('image')->storeAs($folder, $name,config('voyager.storage.disk'));
            $post_model->save();
        }

        return redirect()
          ->route("voyager.{$dataType->slug}.index")
          ->with([
            'message'    => __('voyager.generic.successfully_added_new')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
          ]);
      }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Compatibility with Model binding.
      $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

      $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

      // Check permission
      $this->authorize('edit', $data);

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->editRows);

      if ($val->fails()) {
        return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->ajax()) {
        $post_model = $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
          Log::info('post');
        if($request->file('image')){
            $path_arr = explode('/', $post_model->image);
            $ext_arr = explode('.',$post_model->image);
            $ext = end($ext_arr);
            $old_name = end($path_arr);
            $name = str_replace('.' . $ext,'-original.' . $ext,end($path_arr));
            Log::info('image name: ' . $name);
            $folder = str_replace($old_name,'',$post_model->image);
            $request->file('image')->storeAs($folder, $name,config('voyager.storage.disk'));
            $post_model->save();
        }
        
        return redirect()
          ->route("voyager.{$dataType->slug}.index")
          ->with([
            'message'    => __('voyager.generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
          ]);
      }
    }
    public function grid(Request $request){
        $categories = Category::all();
        return view('grid.edit',['categories' => $categories]);
    }
}
