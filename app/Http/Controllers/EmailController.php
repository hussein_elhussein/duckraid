<?php

namespace App\Http\Controllers;

use App\Email;
use App\Mail\Inquiry;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EmailController extends Controller
{
    public function send(Request $request)
    {
        $validator = $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'string',
            'email' => 'required|email',
            'phone' => 'numeric',
            'subject' => 'string',
            'body' => 'required|string',
            'attachment' => 'string',
            'inquiry_type' => 'required|string'
        ]);
        //save to db:
        $email = new Email();
        $email->fill($request->toArray());
        $email->save();
        Mail::queue(new Inquiry($email,$request->inquiry_type));
        return response()->json('ok');
    }
    public function attachment(Request $request)
    {
        $request->validate([
            'attachment' => 'required|file'
        ]);
        $path = Storage::putFile('attachments', $request->attachment);
        return response()->json($path);
    }
}
