<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiPostController extends Controller
{
  public function allWork(){
      //todo: edit pagination to 10
    $posts = Post::where([['site_header',0],['status','PUBLISHED']])
        ->with('miniCategory')
        ->orderBy('in_all_order')
        ->paginate(10);
    return response()->json($posts);
  }
    public function allWorkNoPag(){
        //todo: edit pagination to 10
        $posts = Post::where([['site_header',0],['status','PUBLISHED']])
            ->with('miniCategory')
            ->orderBy('in_all_order')
            ->get();
        return response()->json($posts);
    }
  public function siteHeaders(){
    $posts = Post::where([['site_header',1],['status','PUBLISHED']])->get();
    return $posts;
  }
  public function category($name){
      $cat = Category::where('name', 'like', '%' . $name . '%')->first();
      if($cat){

          $posts = $cat->posts()
              ->with('miniCategory')
              ->where([['site_header',0],['status','PUBLISHED']])
              ->orderBy('order')
              ->paginate(10);
          return response()->json($posts);
      }
      return response()->json(null,404);
  }
  public function categoryById($id){
        $cat = Category::findOrFail($id);
        if($cat){
            $posts = $cat->posts()
                ->with('miniCategory')
                ->where([['site_header',0],['status','PUBLISHED']])
                ->orderBy('order')->get();
            return response()->json($posts);
        }
        return response()->json(null,404);
    }
  public function catNavs(){
    $cats = Category::all();
    $cats_mod = [];
    foreach ($cats as $cat) {
      $cat_mod = $cat->toArray();
      if(!empty($cat->posts->toArray())){
        $cat_mod['name'] = strtolower($cat->name);
        array_push($cats_mod,$cat_mod);
      }

    }
    return $cats_mod;
  }
  public function landingCat(){
    $cat = Category::whereLanding(1)->get()->first();
    if(!$cat){
        return redirect('api/post/all-work');
    }
    $posts = $cat
        ->posts()
        ->with('miniCategory')
        ->where([['site_header',0],['status','PUBLISHED']])
        ->orderBy('order')
        ->paginate(10);
    return response()->json($posts);
  }
  public function updateGrid(Request $request){
      $posts = $request->toArray();
      for($i = 0; $i < count($posts);$i++ ){
          $post = Post::findOrFail($posts[$i]['id']);
          $post->columns = $posts[$i]['columns'];
          $post->rows = $posts[$i]['rows'];
          $post->order = $posts[$i]['order'];
          $post->in_all_columns = $posts[$i]['in_all_columns'];
          $post->in_all_rows = $posts[$i]['in_all_rows'];
          $post->in_all_order = $posts[$i]['in_all_order'];
          $post->update();
      }
      return response()->json('ok');
  }
  public function getImage($post,$header = false){
    $path = str_replace('\\', "/",$post['image']);
    $ports = explode('.',$path);
    $ext = $ports[count($ports) - 1];
    $target_path = null;
    if($post['columns'] < 4 && !$header){
        $target_path = str_replace("." . $ext,'-medium.' . $ext,$path);
    }else{
        $target_path = str_replace("." . $ext,'-original.' . $ext,$path);
    }
      $contents = Storage::get('public/' . $target_path);
      $base64 = "data:image/" . $ext . ";base64, ". base64_encode($contents);
      return $base64;
  }
}
