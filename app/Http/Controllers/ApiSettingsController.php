<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class ApiSettingsController extends Controller
{
    public function siteSettings(){
      $settings = setting('site');
      return response()->json($settings);

    }
    public function social(){
        $social = setting('social-pages');
        return response()->json($social);
    }
    public function contactDetails(){
        $contact_details = setting('contact-details');
        return response()->json($contact_details);
    }
    public function about(){
        $page = Page::where('title', 'like', '%about%')
            ->first();
        return response()->json($page);
    }
}
