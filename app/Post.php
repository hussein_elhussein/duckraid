<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    protected $hidden = [
        'author_id',
        'excerpt',
        'pull',
        'status',
        'created_at',
        'updated_at',
        'pivot'
    ];
    public function category(){
        return $this->belongsToMany('App\Category');
    }
    public function miniCategory(){
        return $this->belongsToMany('App\Category')->select('categories.id','categories.name');
    }

}
