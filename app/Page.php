<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $hidden = [
        'id',
        'author_id',
        'excerpt',
        'image',
        'slug',
        'status',
        'created_at',
        'updated_at'
    ];
}
