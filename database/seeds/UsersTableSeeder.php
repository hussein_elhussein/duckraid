<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'Hussein',
                'email' => 'function.op@gmail.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$QqgPo4DaTatzk9dPfLE2r.Eb5X6A0o.NlBJqbYj9.3fFJ36eJxjtW',
                'remember_token' => 'YjEvup6hlVWIdnZkkrRCOJnwXmkC5AYnQ2o2SQWGzGB61ABt6spUcBaExyMV',
                'created_at' => '2018-03-31 02:36:35',
                'updated_at' => '2018-03-31 02:36:35',
            ),
        ));
        
        
    }
}