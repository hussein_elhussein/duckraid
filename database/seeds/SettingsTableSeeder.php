<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'site.title',
                'display_name' => 'Site Title',
                'value' => 'Duckraid',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Site',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'site.description',
                'display_name' => 'Site Description',
                'value' => 'Boutique Create Agency',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Site',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'site.logo',
                'display_name' => 'Site Logo',
                'value' => 'settings/logo.png',
                'details' => '',
                'type' => 'image',
                'order' => 16,
                'group' => 'Site',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'site.google_analytics_tracking_id',
                'display_name' => 'Google Analytics Tracking ID',
                'value' => '',
                'details' => '',
                'type' => 'text',
                'order' => 17,
                'group' => 'Site',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'admin.bg_image',
                'display_name' => 'Admin Background Image',
                'value' => 'settings/background.jpg',
                'details' => '',
                'type' => 'image',
                'order' => 5,
                'group' => 'Admin',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'admin.title',
                'display_name' => 'Admin Title',
                'value' => 'Duckraid',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'admin.description',
                'display_name' => 'Admin Description',
                'value' => 'Boutique Create Agency',
                'details' => '',
                'type' => 'text',
                'order' => 2,
                'group' => 'Admin',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'admin.loader',
                'display_name' => 'Admin Loader',
                'value' => '',
                'details' => '',
                'type' => 'image',
                'order' => 3,
                'group' => 'Admin',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'admin.icon_image',
                'display_name' => 'Admin Icon Image',
                'value' => 'settings/logo.png',
                'details' => '',
                'type' => 'image',
                'order' => 4,
                'group' => 'Admin',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'admin.google_analytics_client_id',
            'display_name' => 'Google Analytics Client ID (used for admin dashboard)',
                'value' => '',
                'details' => '',
                'type' => 'text',
                'order' => 1,
                'group' => 'Admin',
            ),
            10 => 
            array (
                'id' => 14,
                'key' => 'contact-details.inquiry_email',
                'display_name' => 'Send inquiries to',
                'value' => 'inquiries@duckraid.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 2,
                'group' => 'Contact Details',
            ),
            11 => 
            array (
                'id' => 15,
                'key' => 'contact-details.jobs_email',
                'display_name' => 'Send jobs\' inquiries to',
                'value' => 'jobs@duckraid.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 3,
                'group' => 'Contact Details',
            ),
            12 => 
            array (
                'id' => 16,
                'key' => 'social-pages.facebook',
                'display_name' => 'Facebook page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 8,
                'group' => 'Social Pages',
            ),
            13 => 
            array (
                'id' => 17,
                'key' => 'social-pages.twitter',
                'display_name' => 'Twitter page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 9,
                'group' => 'Social Pages',
            ),
            14 => 
            array (
                'id' => 18,
                'key' => 'social-pages.instagram',
                'display_name' => 'Instagram page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 10,
                'group' => 'Social Pages',
            ),
            15 => 
            array (
                'id' => 19,
                'key' => 'social-pages.youtube',
                'display_name' => 'Youtube page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 11,
                'group' => 'Social Pages',
            ),
            16 => 
            array (
                'id' => 20,
                'key' => 'contact-details.message_header',
                'display_name' => 'On submit message header',
                'value' => 'Thank you',
                'details' => NULL,
                'type' => 'text',
                'order' => 9,
                'group' => 'Contact Details',
            ),
            17 => 
            array (
                'id' => 21,
                'key' => 'contact-details.message_content',
                'display_name' => 'On submit message content',
                'value' => 'We will get back to you as soon as possible',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 10,
                'group' => 'Contact Details',
            ),
            18 => 
            array (
                'id' => 22,
                'key' => 'social-pages.vimeo',
                'display_name' => 'Vimeo page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 14,
                'group' => 'Social Pages',
            ),
            19 => 
            array (
                'id' => 23,
                'key' => 'social-pages.linkedin',
                'display_name' => 'Linkedin page',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 15,
                'group' => 'Social Pages',
            ),
            20 => 
            array (
                'id' => 24,
                'key' => 'contact-details.phone',
                'display_name' => 'Phone',
                'value' => '+961 1 486612',
                'details' => NULL,
                'type' => 'text',
                'order' => 7,
                'group' => 'Contact Details',
            ),
            21 => 
            array (
                'id' => 25,
                'key' => 'contact-details.location',
                'display_name' => 'Location',
                'value' => '#',
                'details' => NULL,
                'type' => 'text',
                'order' => 8,
                'group' => 'Contact Details',
            ),
            22 => 
            array (
                'id' => 27,
                'key' => 'contact-details.email',
                'display_name' => 'Email',
                'value' => 'info@duckraid.com',
                'details' => NULL,
                'type' => 'text',
                'order' => 1,
                'group' => 'Contact Details',
            ),
            23 => 
            array (
                'id' => 28,
                'key' => 'contact-details.jobs_text',
                'display_name' => 'Jobs text',
                'value' => 'Please send your CV & portfolio to:',
                'details' => NULL,
                'type' => 'text_area',
                'order' => 4,
                'group' => 'Contact Details',
            ),
            24 => 
            array (
                'id' => 29,
                'key' => 'contact-details.inquiry_text',
                'display_name' => 'Inquiries text',
                'value' => 'Please send your brief & files to:',
                'details' => NULL,
                'type' => 'text',
                'order' => 5,
                'group' => 'Contact Details',
            ),
            25 => 
            array (
                'id' => 31,
                'key' => 'contact-details.address',
                'display_name' => 'Address',
                'value' => '<p>Sin El Fil, Mkalles Roundabout</p>
<p>Gehchan Vanilian BLDG, 5th floor</p>',
                'details' => NULL,
                'type' => 'rich_text_box',
                'order' => 6,
                'group' => 'Contact Details',
            ),
            26 => 
            array (
                'id' => 33,
                'key' => 'site.keywords',
                'display_name' => 'Meta keywords',
                'value' => '',
                'details' => NULL,
                'type' => 'text',
                'order' => 4,
                'group' => 'Site',
            ),
            27 => 
            array (
                'id' => 34,
                'key' => 'contact-details.keywords',
                'display_name' => 'Meta keywords',
                'value' => 'duckraid contact us, contact,jobs opportunities, contact details',
                'details' => NULL,
                'type' => 'text',
                'order' => 18,
                'group' => 'Contact Details',
            ),
            28 => 
            array (
                'id' => 35,
                'key' => 'contact-details.desciption',
                'display_name' => 'Meta description',
                'value' => 'contact us',
                'details' => NULL,
                'type' => 'text',
                'order' => 19,
                'group' => 'Contact Details',
            ),
        ));
        
        
    }
}