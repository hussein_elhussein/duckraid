<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pages')->delete();
        
        \DB::table('pages')->insert(array (
            0 => 
            array (
                'id' => 2,
                'author_id' => 1,
                'title' => 'learn about Duckraid',
                'excerpt' => NULL,
                'body' => '<h3>at Duckraid&nbsp;</h3>
<h3>we ain\'t no sitting ducks!&nbsp;</h3>
<h3>we keep busy creating:&nbsp;</h3>
<ul>
<li>
<h4>. 2D &amp; 3D Animations&nbsp;</h4>
</li>
<li>
<h4>. Architectural Rendering and Animations&nbsp;</h4>
</li>
<li>
<h4>. Motion Design&nbsp;</h4>
</li>
<li>
<h4>. Visual Effects&nbsp;</h4>
</li>
</ul>
<h3>if you\'re looking for a digital arts studio&nbsp;</h3>
<h3>that can help your sales take flight,&nbsp;</h3>
<h3>the hunt is over.&nbsp;</h3>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="copyright" style="text-align: center;">&copy; 2018 duckraid, all right reserved to duckraid sal</p>',
                'image' => NULL,
                'slug' => 'lear',
                'meta_description' => NULL,
                'meta_keywords' => NULL,
                'status' => 'ACTIVE',
                'created_at' => '2018-04-04 12:54:32',
                'updated_at' => '2018-04-04 13:02:27',
            ),
        ));
        
        
    }
}