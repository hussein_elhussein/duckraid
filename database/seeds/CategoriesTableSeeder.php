<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 2,
                'parent_id' => NULL,
                'order' => 4,
                'landing' => 0,
                'name' => 'Stills',
                'slug' => 'stills',
                'description' => 'stills',
                'keywords' => 'stills',
                'created_at' => NULL,
                'updated_at' => '2018-04-07 01:05:04',
            ),
            1 => 
            array (
                'id' => 3,
                'parent_id' => NULL,
                'order' => 5,
                'landing' => 0,
                'name' => 'Animation',
                'slug' => 'animation',
                'description' => '',
                'keywords' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'parent_id' => NULL,
                'order' => 6,
                'landing' => 0,
                'name' => 'Motion Graphic',
                'slug' => 'motion-graphic',
                'description' => '',
                'keywords' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'parent_id' => NULL,
                'order' => 7,
                'landing' => 0,
                'name' => 'Reel',
                'slug' => 'reel',
                'description' => '',
                'keywords' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'parent_id' => NULL,
                'order' => 8,
                'landing' => 0,
                'name' => 'Design',
                'slug' => 'design',
                'description' => '',
                'keywords' => '',
                'created_at' => NULL,
                'updated_at' => '2018-03-19 00:39:15',
            ),
        ));
        
        
    }
}