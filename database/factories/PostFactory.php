<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
      'author_id' => 1,
      'columns' => $faker->randomElement([1,4]),
      'in_all_columns' => $faker->randomElement([1,4]),
      'order' => $faker->numberBetween(1,50),
      'image' => $faker->randomElement(['posts/1.jpg','posts/2.jpg','posts/3.jpg','posts/4.jpg','posts/5.jpg']),
      'title' => $faker->text(50),
      'excerpt' => $faker->text(50),
      'body' => $faker->text(100),
      'slug' => $faker->unique()->text(50),
      'meta_description' => $faker->text(10),
      'meta_keywords' => $faker->text(10),
      'status' => "PUBLISHED",
      'post_group_id' => $faker->numberBetween(1,50),
      'featured' => $faker->numberBetween(0,1),
    ];
});
