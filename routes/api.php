<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('post/all-work',"ApiPostController@allWork");
Route::get('category/all-work',"ApiPostController@allWorkNoPag");
Route::get('category/{id}',"ApiPostController@categoryById");
Route::get('post/headers',"ApiPostController@siteHeaders");
Route::post('post/update_grid','ApiPostController@updateGrid');
Route::get('post/category/navs',"ApiPostController@catNavs");
Route::get('post/category/landing',"ApiPostController@landingCat");
Route::get('post/category/{name}',"ApiPostController@category");
Route::get('settings/site',"ApiSettingsController@siteSettings");
Route::get('settings/social',"ApiSettingsController@social");
Route::get('settings/contact_details',"ApiSettingsController@contactDetails");
Route::get('settings/about',"ApiSettingsController@about");
Route::post('email/send',"EmailController@send");
Route::post('email/attachment',"EmailController@attachment");