<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Voyager::routes();

Route::get('/grid','PostController@grid');
Route::get('/admin/{resource}',function ($resource){
    return redirect('/' . $resource);
})->where(['resource' => '.*']);
Route::post('/admin/{resource}',function ($resource){
    return redirect('/' . $resource);
})->where(['resource' => '.*']);

