require('./gallery');
window.Vue = require('vue');
import vueResource from 'vue-resource';
import postsComponent from './components/posts-component.vue';
Vue.use(vueResource);
const app = new Vue({
    el: '#app',
    components: {
        'posts': postsComponent
    },
    methods: {
        generateGrid() {
            var cat = $("select[name='category_id']" ).val();
            var all_work = $("input[name='all_work']")[0].checked;
            if(all_work){
                this.$refs.postsComponent.getPosts(null,true);
            }else{
                this.$refs.postsComponent.getPosts(cat);
            }

        },
        saveGrid(){
            this.$refs.postsComponent.saveGrid();
        }
    }
});